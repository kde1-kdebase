# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# Marcell Lengyel <miketkf@yahoo.com>, 1998.
#
msgid ""
msgstr ""
"Project-Id-Version: kdebase 981215\n"
"POT-Creation-Date: 1999-01-08 01:16+0100\n"
"PO-Revision-Date: 1998-12-17 16:23+0100\n"
"Last-Translator: Marcell Lengyel <miketkf@yahoo.com>\n"
"Language-Team: Hungarian <hu@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8869-2\n"
"Content-Transfer-Encoding: 8-bit\n"

#: main.cpp:72
msgid "&Standard shortcuts"
msgstr "&Alapvet� r�vid�t�sek"

#: main.cpp:80
msgid "&Global shortcuts"
msgstr "&Glob�lis r�vid�t�sek"

#: main.cpp:88
msgid "usage: kcmkeys [-init | {standard,global}]\n"
msgstr "haszn�lat: kcmkeys [-init | {standard,global}]\n"

#: main.cpp:132
msgid "Key binding settings"
msgstr "Billenty� k�t�sek be�ll�t�sa"

#: keyconfig.cpp:106
msgid "Current scheme"
msgstr "Jelenlegi �ssze�ll�t�s"

#: keyconfig.cpp:108
msgid "KDE default"
msgstr "KDE alap�rtelmezett"

#: keyconfig.cpp:115
msgid "&Key scheme"
msgstr "&Bilenty� k�t�sek"

#: keyconfig.cpp:126
msgid "&Add ..."
msgstr "Hozz�&ad�s..."

#: keyconfig.cpp:148
msgid "&Save changes"
msgstr "V�ltoztat�&sok Ment�se"

#: keyconfig.cpp:211
msgid "Error removing scheme"
msgstr "Hiba az �ssze�ll�t�s t�rl�sekor"

#: keyconfig.cpp:212
msgid ""
"This key scheme could not be removed.\n"
"Perhaps you do not have permission to alter the file\n"
"system where the key scheme is stored."
msgstr ""
"Ez az �ssze�ll�t�s nem t�vol�that� el.\n"
"Tal�n nincsen jogosults�ga az �llom�ny megv�ltoztat�s�ra"

#: keyconfig.cpp:343
msgid "Naming conflict"
msgstr "Elnevez�si hiba"

#: keyconfig.cpp:344
msgid ""
"Please choose a unique name for the new key\n"
"scheme. The one you entered already appears\n"
"in the key scheme list."
msgstr ""
"K�rem v�lasszon egy egyedi nevet az �j �ssze�ll�t�s\n"
"sz�m�ra. Amit megadott, az m�r szerepel a list�ban."

#: savescm.cpp:18
msgid "Add a key scheme"
msgstr "�ssze�ll�t�s hozz�ad�sa"

#: savescm.cpp:32
msgid ""
"&Enter a name for the new key scheme\n"
"to be added to your personal list.\n"
"\n"
"The keys currently used in the preview will\n"
"be copied into this scheme to begin with."
msgstr ""
"Adjon meg egy nevet az �j �ssze�ll�t�s sz�m�ra\n"
"\n"
"A billenty�k, amiket l�t bem�sol�dnak az \n"
"�j �ssze�ll�t�sba kiindul�snak."

#: ../../kwm/kwmbindings.cpp:1
msgid "Task manager"
msgstr "Feladat vez�rl�"

#: ../../kwm/kwmbindings.cpp:2
msgid "Kill window mode"
msgstr "Ablak elpuszt�t�si m�d"

#: ../../kwm/kwmbindings.cpp:3
msgid "Execute command"
msgstr "Parancs v�grehajt�s"

#: ../../kwm/kwmbindings.cpp:4
msgid "Window operations menu"
msgstr "Ablak m�veletek men�"

#: ../../kwm/kwmbindings.cpp:5
msgid "Pop-up system menu"
msgstr "Felbukkan� rendszer men�"

#: ../../kwm/kwmbindings.cpp:7
msgid "Window raise"
msgstr "Ablak el�rehoz�s"

#: ../../kwm/kwmbindings.cpp:8
msgid "Window lower"
msgstr "Ablak h�tra k�ld�s"

#: ../../kwm/kwmbindings.cpp:9
msgid "Window close"
msgstr "Ablak bez�r�s"

#: ../../kwm/kwmbindings.cpp:10
msgid "Window iconify"
msgstr "Ablak ikoniz�l�s"

#: ../../kwm/kwmbindings.cpp:11
msgid "Window resize"
msgstr "Ablak �tm�retez�s"

#: ../../kwm/kwmbindings.cpp:12
msgid "Window move"
msgstr "Ablak mozgat�s"

#: ../../kwm/kwmbindings.cpp:13
msgid "Window toggle sticky"
msgstr "Ablakba rajzsz�g ki/besz�r�s"

#: ../../kwm/kwmbindings.cpp:15
msgid "Switch to desktop 1"
msgstr "1. Asztalra v�lt�s"

#: ../../kwm/kwmbindings.cpp:16
msgid "Switch to desktop 2"
msgstr "2. Asztalra v�lt�s"

#: ../../kwm/kwmbindings.cpp:17
msgid "Switch to desktop 3"
msgstr "3. Asztalra v�lt�s"

#: ../../kwm/kwmbindings.cpp:18
msgid "Switch to desktop 4"
msgstr "4. Asztalra v�lt�s"

#: ../../kwm/kwmbindings.cpp:19
msgid "Switch to desktop 5"
msgstr "5. Asztalra v�lt�s"

#: ../../kwm/kwmbindings.cpp:20
msgid "Switch to desktop 6"
msgstr "6. Asztalra v�lt�s"

#: ../../kwm/kwmbindings.cpp:21
msgid "Switch to desktop 7"
msgstr "7. Asztalra v�lt�s"

#: ../../kwm/kwmbindings.cpp:22
msgid "Switch to desktop 8"
msgstr "8. Asztalra v�lt�s"

#: ../../kwm/kwmbindings.cpp:24
msgid "Switch one desktop to the right"
msgstr "Egy asztallal jobbra"

#: ../../kwm/kwmbindings.cpp:25
msgid "Switch one desktop to the left"
msgstr "Egy asztallal balra"

#: ../../kwm/kwmbindings.cpp:26
msgid "Switch one desktop up"
msgstr "Egy asztallal feljebb"

#: ../../kwm/kwmbindings.cpp:27
msgid "Switch one desktop down"
msgstr "Egy asztallal lejjebb"

#: stdbindings.cpp:3
msgid "New"
msgstr "�j"

#: stdbindings.cpp:8
msgid "Next"
msgstr "K�vetkez�"

#: stdbindings.cpp:9
msgid "Prior"
msgstr "El�z�"

#: stdbindings.cpp:14
msgid "Find"
msgstr "Keres�s"

#: stdbindings.cpp:17
msgid "Home"
msgstr "Elej�re"

#: stdbindings.cpp:18
msgid "End"
msgstr "V�g�re"
